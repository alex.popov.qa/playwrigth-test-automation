# E2E testing the "Sign Up"/"Sign In"/"Profile" of the gambling site

## There is the gambling webapp
- The application must contain a registration form where users can sign up for a new account.
- The registration form must include input fields for email, password, confirmation password, phone number, partner code and age confirmation checkbox.
- Upon successful registration, users should be redirected to the login page.
- Users should receive a confirmation email after registration to verify their email address.

## Task
- Automate the process of testing the "Sign Up"/"Sign In"/"Profile" pages.

## Features to test:
### Register form:
- Automate the process of testing the "Register" form
- Verify that a new user can be successfully registered using correct data.
- Ensure that the mandatory fields cannot be left empty.
- Validate that error messages are displayed if data is entered incorrectly or does not meet the requirements.
- Confirm that after successful registration, the user is automatically logged into the system.

### Sign In/Signout:
- Validate that the user can successfully log into the system using the correct credentials.
- Ensure that after logging out, the user is redirected to the login page.
- Verify that a message or confirmation is displayed upon logout.

### Profile:
- Confirm that the user can view their profile with all relevant information.
- Validate that the user can edit their profile and save the changes.
- Ensure that the user can delete or deactivate their profile.
- Verify that a message or confirmation is displayed before deleting or deactivating the profile.

## Test

### Positive:
#### myProfile:
- TC201, Check an email in the profile
- TC202, Check the "Notification message" toggle switch
- TC203, Check the "Notification push" toggle switch
- TC204, Check profile logout

#### registration:
- TC001, Check successful registration
- TC002, 50 signs password
- TC003, an email with special character
- TC004, change password for the new user

#### signIn:
- TC101, sign-in into the system
- TC102, check an email with special characters
- TC103, check the remember me checkbox

### Negative:
#### regEmail:
- TC005, an email without @ sign
- TC006, an email with 2 @ signs
- TC007, an email without domain
- TC008, an email exist in the DB

#### regPassword:
- TC009, mismatch "ConfirmPassword" with password
- TC010, the password without digits
- TC011, the password with space
- TC012, the password with no English letters
- TC013, the password with only capital letters
- TC014, the password with less than 8 symbols
- TC015, the passport with over 50 symbols
- TC016, the empty password field

#### regPhone:
- TC017, over 9 digits in the phone number
- TC018, less than 9 digits in the phone number
- TC019, symbols in the phone number
- TC020, the phone exist in the DB
- TC021, age checkbox isn`t active

#### signInFailed:
- TC104, Registered user with incorrect password
- TC105, User without registration
- TC106, Sign In without an email
- TC107, Sign In without the password


## Installation

### Steps to install project:

1. Cloning the repository
```shell
git clone https://gitlab.com/alex.popov.qa/playwright-test-automation.git
```

2. Move to directory
```shell
cd playwright-test-automation
```

3. Installing required dependencies
```shell
npm install
```

4. Installing playwright
```shell
npm install playwright
```

5. Installing dotenv dependencies
```shell
npm install dotenv --save
```

# Command Line

## Running all tests

```shell
npx playwright test
```

## Running a single test file

```shell
npx playwright test positive/signIn.spec.ts
```

## Run a set of test files

```shell
npx playwright test positive/ negative/
```

## Run files that have create or edit in the file name

```shell
npx playwright test create edit
```

## Run the test with the title

```shell
npx playwright test -g "should allow to add todo item"
```

## Running tests in headed mode

```shell
npx playwright test positive/signIn.spec.ts --headed
```

## Running tests on a specific project

```shell
npx playwright test positive/signIn.spec.ts --project=firefox
```

## Running tests in the debug mode

```shell
npx playwright test --project=firefox --debug
```
