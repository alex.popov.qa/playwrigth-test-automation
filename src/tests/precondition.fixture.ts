import {test as base} from "@playwright/test";
import {HomePage} from "../pages/HomePage";
import {MyProfile} from "../pages/MyProfile";
import {RegisterPage} from "../pages/RegisterPage";
import {SignInPage} from "../pages/SignInPage";

type pages = {
  homePage: HomePage;
  myProfile: MyProfile;
  registerPage: RegisterPage;
  signInPage: SignInPage;
}

const testPages = base.extend<pages>({
  homePage: async ({page}, use) => {
    await use(new HomePage(page));
  },
  myProfile: async ({page}, use) => {
    await use(new MyProfile(page));
  },
  registerPage: async ({page}, use) => {
    await use(new RegisterPage(page));
  },
  signInPage: async ({page}, use) => {
    await use(new SignInPage(page));
  }
})

base.beforeEach(async ({page}) => {
  await page.goto('en/');
});

export const test = testPages;
export const expect = testPages.expect;
