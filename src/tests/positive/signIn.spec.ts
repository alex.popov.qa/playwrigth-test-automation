import {test} from '../precondition.fixture'

test.describe('Positive Scenario: Sign In into the system', () => {

  test.beforeEach(async ({homePage}) => {
    await homePage.clickSignInButton();
  })

  test.afterEach(async ({signInPage, homePage}) => {
    await signInPage.clickSignInButton();
    await signInPage.clickCloseVerificationPopUp();
    await homePage.isProfileIconIsVisible();
  })

  test('TC101, sign-in into the system', async ({signInPage}) => {
    await signInPage.fillSignInForm(process.env.USER_EMAIL, process.env.USER_PASSWORD)
  })

  test('TC102, check an email with special characters', async ({signInPage}) => {
    await signInPage.fillSignInForm(process.env.USER_EMAIL.replace("@", "+te.st+example@"), process.env.USER_PASSWORD)
  })

  test('TC103, check the remember me checkbox', async ({signInPage}) => {
    await signInPage.fillSignInForm(process.env.USER_EMAIL, process.env.USER_PASSWORD)
    await signInPage.clickRememberMeCheckboxTrue();
  })
})
