import {test} from '../precondition.fixture'

test.describe('Positive Scenario: register new account', () => {

  test.beforeEach(async ({homePage}) => {
    await homePage.clickRegisterButton();
  })

  test('TC001, Check successful registration', async ({registerPage, homePage, myProfile}) => {
    let datestamp = Date.now().toString().slice(-6);
    let email = process.env.REGISTER_EMAIL.replace("@", "+" + datestamp + "@");
    await registerPage.fillRegistrationForm(
      email,
      'Password123!',
      'Password123!',
      datestamp,
      process.env.REGISTER_PROMO_CODE,
      true);
    await registerPage.clickConfirmRegisterButton();
    await homePage.clickWelcomeMessageOk();
    await homePage.clickMyProfile();
    await myProfile.checkProfileEmail(email);
  })

  test('TC002, 50 signs password', async ({registerPage, homePage, myProfile}) => {
    let datestamp = Date.now().toString().slice(-6);
    let email = process.env.REGISTER_EMAIL.replace("@", "+" + datestamp + "@");
    await registerPage.fillRegistrationFormWithDefaultData({
      email: email,
      password: 'TheGreatestPasswordInTheWorldWillBeUsedHereToday1!',
      confirmPassword: 'TheGreatestPasswordInTheWorldWillBeUsedHereToday1!',
      phoneNumber: datestamp
    });
    await registerPage.clickConfirmRegisterButton();
    await homePage.clickWelcomeMessageOk();
    await homePage.clickMyProfile();
    await myProfile.checkProfileEmail(email);
  })

  test('TC003, an email with special character', async ({registerPage, homePage, myProfile}) => {
    let datestamp = Date.now().toString().slice(-6);
    let email = process.env.REGISTER_EMAIL.replace("@", "+" + datestamp + "+te.st+example@");

    await registerPage.fillRegistrationFormWithDefaultData({
      email: email,
      phoneNumber: datestamp
    })
    await registerPage.clickConfirmRegisterButton();
    await homePage.clickWelcomeMessageOk();
    await homePage.clickMyProfile();
    await myProfile.checkProfileEmail(email);
  })

  test('TC004, change password for the new user', async ({registerPage, homePage, myProfile, signInPage}) => {
    let datestamp = Date.now().toString().slice(-6);
    let email = process.env.REGISTER_EMAIL.replace("@", "+" + datestamp + "@");
    let password = '!Password123'
    let passwordNew = "!Password123!@#"
    await registerPage.fillRegistrationFormWithDefaultData({
      email: email,
      phoneNumber: datestamp,
      password: password,
      confirmPassword: password
    })
    await registerPage.clickConfirmRegisterButton();
    await homePage.clickWelcomeMessageOk();
    await homePage.clickMyProfile();
    await myProfile.changePasswordForm(password, passwordNew, passwordNew);

    await signInPage.fillSignInForm(email, passwordNew)
    await signInPage.clickSignInButton();
    await signInPage.clickCloseVerificationPopUp();
    await homePage.isProfileIconIsVisible();
  })
})
