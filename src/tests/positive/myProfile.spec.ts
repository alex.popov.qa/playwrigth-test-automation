import {expect, test} from '../precondition.fixture'

test.describe('Positive Scenario: Sign In into the system', () => {

  test.beforeEach(async ({page, homePage, signInPage}) => {
    await homePage.clickSignInButton();
    await signInPage.fillSignInForm(process.env.USER_EMAIL, process.env.USER_PASSWORD)
    await signInPage.clickSignInButton();
    await page.waitForTimeout(5000);
    await signInPage.clickCloseVerificationPopUp();
    await homePage.clickMyProfile();
  })

  test('TC201, Check an email in the profile', async ({myProfile}) => {
    await myProfile.checkProfileEmail(process.env.USER_EMAIL);
  })

  test('TC202, Check the "Notification message" toggle switch', async ({myProfile}) => {
    let check = await myProfile.notificationMessageYes();
    await myProfile.switchNotificationMessage();
    await myProfile.clickSaveChangesButton();
    expect(check).not.toEqual(await myProfile.notificationMessageYes())
  })

  test('TC203, Check the "Notification push" toggle switch', async ({myProfile}) => {
    let check = await myProfile.notificationPushYes();
    await myProfile.switchNotificationPush();
    await myProfile.clickSaveChangesButton();
    expect(check).not.toEqual(await myProfile.notificationPushYes())
  })

  test('TC204, Check profile logout', async ({myProfile, homePage}) => {
    await myProfile.clickCloseProfileButton()
    await homePage.clickProfileLogout();
    expect(homePage.isProfileIconIsVisible()).toBeFalsy();
  })
})
