import {test} from '../precondition.fixture'

test.describe('Negative Scenario: check an email field validation', () => {

  test.beforeEach(async ({homePage}) => {
    await homePage.clickRegisterButton()
  })

  test('TC005, an email without @ sign', async ({registerPage}) => {
    await registerPage.fillRegistrationFormWithDefaultData({
      email: process.env.REGISTER_EMAIL.replace("@", "")
    })
    await registerPage.clickConfirmRegisterButton();
    await registerPage.assertEmailErrorVisible();
  })

  test('TC006, an email with 2 @ signs', async ({registerPage}) => {
    await registerPage.fillRegistrationFormWithDefaultData({
      email: process.env.REGISTER_EMAIL.replace("@", "@test@")
    })
    await registerPage.clickConfirmRegisterButton();
    await registerPage.assertEmailErrorVisible();
  })

  test('TC007, an email without domain', async ({registerPage}) => {
    await registerPage.fillRegistrationFormWithDefaultData({
      email: process.env.REGISTER_EMAIL.substring(0, process.env.REGISTER_EMAIL.indexOf("@"))
    })
    await registerPage.clickConfirmRegisterButton();
    await registerPage.assertEmailErrorVisible();
  })

  test('TC008, an email exist in the DB', async ({registerPage}) => {
    let datestamp = Date.now().toString().slice(-6);
    await registerPage.fillRegistrationFormWithDefaultData({
      email: 'test@example.com',
      phoneNumber: datestamp
    })
    await registerPage.clickConfirmRegisterButton();
    await registerPage.assertWarningEmailExist();
  })
})
