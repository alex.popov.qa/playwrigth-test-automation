import {test} from '../precondition.fixture'

test.describe('Negative Scenario: check the phone validation', () => {

  test.beforeEach(async ({homePage}) => {
    await homePage.clickRegisterButton();
  })


  test('TC017, over 9 digits in the phone number', async ({registerPage}) => {
    await registerPage.fillRegistrationFormWithDefaultData({
      phoneNumber: '5012345671'
    })
    await registerPage.assertPhoneNumberErrorVisible();
  })

  // The bug with the amount of digits - 9 digits required according to the error message text
  // test('TC018, less than 9 digits in the phone number', async ({ registerPage }) => {
  //   let datestamp = Date.now().toString().slice(-6);
  //   await registerPage.fillRegistrationFormWithDefaultData({
  //     phoneNumber: datestamp
  //   })
  //   await registerPage.assertPhoneNumberErrorVisible();
  // })

  test('TC019, symbols in the phone number', async ({registerPage}) => {
    await registerPage.fillRegistrationFormWithDefaultData({
      phoneNumber: '50123456q'
    })
    await registerPage.assertPhoneNumberErrorVisible();
  })

  test('TC020, the phone exist in the DB', async ({registerPage}) => {
    let datestamp = Date.now().toString().slice(-6);
    let email = process.env.REGISTER_EMAIL.replace("@", "+" + datestamp + "@");
    await registerPage.fillRegistrationFormWithDefaultData({
      email: email,
      phoneNumber: '123456789'
    })
    await registerPage.clickConfirmRegisterButton();
    await registerPage.assertPhoneNumberErrorVisible();
  })

  test('TC021, age checkbox isn`t active', async ({registerPage}) => {
    await registerPage.fillRegistrationFormWithDefaultData({
      ageCheck: false
    })
    await registerPage.assertAgeCheckErrorVisible();
  })
})
