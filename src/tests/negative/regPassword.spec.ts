import {test} from '../precondition.fixture'

test.describe('Negative Scenario: check the password validation', () => {

  test.beforeEach(async ({homePage}) => {
    await homePage.clickRegisterButton();
  })

  test('TC009, mismatch "ConfirmPassword" with password', async ({registerPage}) => {
    await registerPage.fillRegistrationFormWithDefaultData({
      password: 'Password123!',
      confirmPassword: 'Password1'
    })
    await registerPage.assertConfirmPasswordErrorVisible();
  })

  test('TC010, the password without digits', async ({registerPage}) => {
    await registerPage.fillRegistrationFormWithDefaultData({
      password: 'PasswordHello!',
      confirmPassword: 'PasswordHello!'
    })
    await registerPage.assertPasswordValidation('invalid');
  })

  test('TC011, the password with space', async ({registerPage}) => {
    await registerPage.fillRegistrationFormWithDefaultData({
      password: 'Password 123!',
      confirmPassword: 'Password 123!'
    })
    await registerPage.assertPasswordValidation('invalid');
  })

  test('TC012, the password with no English letters', async ({registerPage}) => {
    await registerPage.fillRegistrationFormWithDefaultData({
      password: 'Пароль123!',
      confirmPassword: 'Пароль123!'
    })
    await registerPage.assertPasswordValidation('invalid');
  })

  test('TC013, the password with only capital letters', async ({registerPage}) => {
    await registerPage.fillRegistrationFormWithDefaultData({
      password: 'PASSWORD123!',
      confirmPassword: 'PASSWORD123!'
    })
  })

  test('TC014, the password with less than 8 symbols', async ({registerPage}) => {
    await registerPage.fillRegistrationFormWithDefaultData({
      password: 'PAS123!',
      confirmPassword: 'PAS123!'
    })
    await registerPage.assertPasswordValidation('invalid');
  })

  test('TC015, the passport with over 50 symbols', async ({registerPage}) => {
    await registerPage.fillRegistrationFormWithDefaultData({
      password: 'TheGreatestPasswordInTheWorldWillBeUsedHereToday1!@',
      confirmPassword: 'TheGreatestPasswordInTheWorldWillBeUsedHereToday1!@'
    })
    await registerPage.assertPasswordValidation('invalid');
  })

  test('TC016, the empty password field', async ({registerPage}) => {
    await registerPage.fillRegistrationFormWithDefaultData({
      password: '',
      confirmPassword: ''
    })
    await registerPage.assertPasswordValidation('invalid');
  })
})
