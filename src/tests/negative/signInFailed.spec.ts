import {test} from '../precondition.fixture'

test.describe('Negative Scenario: Sign In into the system', () => {

  test.beforeEach(async ({homePage}) => {
    await homePage.clickSignInButton();
  })

  test('TC104, Registered user with incorrect password', async ({signInPage}) => {
    await signInPage.fillSignInForm(process.env.USER_EMAIL, process.env.USER_PASSWORD + "qwe")
    await signInPage.clickSignInButton();
    await signInPage.assertSignInErrorVisible();
  })

  test('TC105, User without registration', async ({signInPage}) => {
    await signInPage.fillSignInForm(process.env.REGISTER_EMAIL.replace("@", "+test@"), process.env.USER_PASSWORD)
    await signInPage.clickSignInButton();
    await signInPage.assertSignInErrorVisible();
  })

  test('TC106, Sign In without an email', async ({signInPage}) => {
    await signInPage.fillSignInForm('', process.env.USER_PASSWORD)
    await signInPage.clickSignInButton();
    await signInPage.assertRequiredEmailErrorVisible();
  })

  test('TC107, Sign In without the password', async ({signInPage}) => {
    await signInPage.fillSignInForm(process.env.USER_EMAIL, '')
    await signInPage.clickSignInButton();
    await signInPage.assertRequiredPasswordErrorVisible();
  })
})
