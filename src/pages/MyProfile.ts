import {expect, Locator, Page} from '@playwright/test'

export class MyProfile {
  private page: Page

  private readonly internalMessageYes: Locator
  private readonly internalMessageNo: Locator
  private readonly pushNotificationYes: Locator
  private readonly pushNotificationNo: Locator

  private readonly profileEmail: Locator
  private readonly passwordField: Locator
  private readonly saveChangesButton: Locator
  private readonly closeProfileButton: Locator

  private readonly changePassword: Locator
  private readonly changePasswordCurrent: Locator
  private readonly changePasswordNew: Locator
  private readonly changePasswordNewConfirm: Locator
  private readonly changePasswordSubmit: Locator
  private readonly changePasswordSuccessMessage: Locator

  constructor(page: Page) {
    this.page = page

    this.internalMessageYes = this.page.locator('//input[@name="subscribe_to_internal_message"]/..//p[text()="Yes"]')
    this.internalMessageNo = this.page.locator('//input[@name="subscribe_to_internal_message"]/..//p[text()="No"]')
    this.pushNotificationYes = this.page.locator('//input[@name="subscribe_to_push_notification"]/..//p[text()="Yes"]')
    this.pushNotificationNo = this.page.locator('//input[@name="subscribe_to_push_notification"]/..//p[text()="No"]')
    this.profileEmail = this.page.locator('//input[@name="email"]')

    this.passwordField = this.page.locator('//input[@name="password"]')
    this.saveChangesButton = this.page.locator('//span[normalize-space()="Save changes"]')
    this.closeProfileButton = this.page.locator('(//i[@class="e-p-close-icon-bc bc-i-close-remove"])[2]')

    this.changePassword = this.page.locator('//span[normalize-space()="Change Password"]')
    this.changePasswordCurrent = this.page.locator('//input[@name="password"]')
    this.changePasswordNew = this.page.locator('//input[@name="new_password"]')
    this.changePasswordNewConfirm = this.page.locator('//input[@name="new_password_confirm"]')
    this.changePasswordSubmit = this.page.locator('//button[@title="Change Password"]')
    this.changePasswordSuccessMessage = this.page.locator('//button[contains(@title,"Ok")]')
  }

  async notificationMessageYes(): Promise<boolean> {
    return this.internalMessageYes.isChecked().then((isChecked) => isChecked).catch(() => false);
  }

  async switchNotificationMessage(): Promise<boolean> {
    await this.internalMessageYes.isChecked()
      ? await this.internalMessageNo.check()
      : await this.internalMessageYes.check();
    return this.internalMessageYes.isChecked()
  }

  async notificationPushYes(): Promise<boolean> {
    return this.pushNotificationYes.isChecked().then((isChecked) => isChecked).catch(() => false);
  }

  async switchNotificationPush(): Promise<boolean> {
    if (await this.pushNotificationYes.isChecked()) {
      await this.pushNotificationNo.check();
      return true
    } else {
      await this.pushNotificationYes.check();
      return false
    }
  }

  async checkProfileEmail(registerEmail: string) {
    const profEmail = await this.getProfileEmail();
    expect(profEmail).toEqual(registerEmail);
  }

  private async getProfileEmail(): Promise<string | null> {
    const element = this.profileEmail;
    return await element.getAttribute('value');
  }

  async clickSaveChangesButton() {
    await this.passwordField.fill(process.env.USER_PASSWORD);
    await this.saveChangesButton.click();
  }

  async clickCloseProfileButton() {
    await this.closeProfileButton.click();
  }

  async changePasswordForm(currentPassword: string, newPassword: string, newPasswordConfirm: string) {
    await this.changePassword.click();
    await this.changePasswordCurrent.fill(currentPassword);
    await this.changePasswordNew.fill(newPassword);
    await this.changePasswordNewConfirm.fill(newPasswordConfirm);
    await this.changePasswordSubmit.click();
    await this.changePasswordSuccessMessage.click();
  }
}

