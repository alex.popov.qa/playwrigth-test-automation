import {expect, Locator, Page} from '@playwright/test'

export class RegisterPage {
  private page: Page

  private readonly email: Locator
  private readonly password: Locator
  private readonly confirmPassword: Locator
  private readonly phoneNumber: Locator
  private readonly partnerCode: Locator
  private readonly ageCheck: Locator

  private readonly failedEmail: Locator
  private readonly failedPassword: Locator
  private readonly failedConfirmPassword: Locator
  private readonly failedPhoneNumber: Locator
  private readonly failedAgeCheck: Locator

  private readonly warningEmailExist: Locator
  private readonly confirmRegisterButton: Locator

  constructor(page: Page) {
    this.page = page

    this.email = this.page.locator('//input[@name="email"]')
    this.password = this.page.locator('//input[@name="password"]')
    this.confirmPassword = this.page.locator('//input[@name="repeat_password"]')
    this.phoneNumber = this.page.locator('//input[@name="phoneNumber"]')
    this.partnerCode = this.page.locator('//input[@name="promo_code"]')
    this.ageCheck = this.page.locator('//i[@class="checkbox-control-icon-bc bc-i-checked"]')

    this.failedEmail = this.page.locator('//input[@name="email"]/../..//span[@class="form-control-message-bc"]')
    this.failedPassword = this.page.locator('//input[@name="password"]/../..//div[@class="validation-content"]//span[last()]')
    this.failedConfirmPassword = this.page.locator('//input[@name="repeat_password"]/../..//span[@class="form-control-message-bc"]')
    this.failedPhoneNumber = this.page.locator('//input[@name="phoneNumber"]/ancestor::div[@class="form-controls-group-bc telephone"]//span[@class="form-control-message-bc"]')
    this.failedAgeCheck = this.page.locator('//i[@class="checkbox-control-icon-bc bc-i-checked"]/../..//span[@class="form-control-message-bc"]')

    this.warningEmailExist = this.page.locator('//button[text()="Continue Registration"]')
    this.confirmRegisterButton = this.page.locator('//button[@type="submit"]')
  }

  async fillRegistrationForm(email: string, password: string, confirmPassword: string, phoneNumber: string, partnerCode: string, ageCheck: boolean) {
    await this.email.fill(email);
    await this.password.fill(password);
    await this.confirmPassword.fill(confirmPassword);
    await this.phoneNumber.fill(phoneNumber);
    await this.partnerCode.fill(partnerCode);
    ageCheck ? await this.ageCheck.check() : await this.ageCheck.uncheck();
  }

  async fillRegistrationFormWithDefaultData(options: {
    email?: string,
    password?: string,
    confirmPassword?: string,
    phoneNumber?: string
    partnerCode?: string,
    ageCheck?: boolean
  }) {
    const {
      email = process.env.REGISTER_EMAIL,
      password = process.env.REGISTER_PASSWORD,
      confirmPassword = process.env.REGISTER_PASSWORD,
      phoneNumber = Date.now().toString().slice(-9),
      partnerCode = process.env.REGISTER_PROMO_CODE,
      ageCheck = true,
    } = options;
    await this.fillRegistrationForm(email, password, confirmPassword, phoneNumber, partnerCode, ageCheck)
  }

  async assertEmailErrorVisible() {
    await expect(this.failedEmail).toBeVisible();
  }

  async assertPasswordValidation(status: 'valid' | 'invalid') {
    const passwordElement = this.page.locator('.validation-progress').nth(-1);
    await expect(passwordElement).toHaveAttribute('class', `validation-progress ${status}`);
  }

  async assertConfirmPasswordErrorVisible() {
    await expect(this.failedConfirmPassword).toBeVisible();
  }

  async assertPhoneNumberErrorVisible() {
    await expect(this.failedPhoneNumber).toBeVisible();
  }

  async assertAgeCheckErrorVisible() {
    await expect(this.failedAgeCheck).toBeVisible();
  }

  async clickConfirmRegisterButton() {
    await this.confirmRegisterButton.click();
  }

  async assertWarningEmailExist() {
    await expect(this.warningEmailExist).toBeVisible();
  }
}
