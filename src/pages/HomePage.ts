import {Locator, Page} from '@playwright/test'

export class HomePage {
  private page: Page

  private readonly signIn: Locator
  private readonly register: Locator
  private readonly welcomeMessage: Locator
  private readonly profileIcon: Locator
  private readonly myProfile: Locator
  private readonly logout: Locator

  constructor(page: Page) {
    this.page = page

    this.signIn = this.page.locator('//button[@title="Sign in"]')
    this.register = this.page.locator('//span[normalize-space()="Register"]')
    this.welcomeMessage = this.page.locator('//i[@class="e-p-close-icon-bc bc-i-close-remove"]');
    this.profileIcon = this.page.locator('//i[@class="hdr-user-avatar-icon-bc bc-i-user user-not-verified"]');
    this.myProfile = this.page.locator('//span[normalize-space()="My Profile"]')
    this.logout = this.page.locator('//button[@title="Logout"]');
  }

  async clickSignInButton() {
    await this.signIn.click();
  }

  async clickRegisterButton() {
    await this.register.click();
  }

  async clickWelcomeMessageOk() {
    await this.welcomeMessage.click();
  }

  async isProfileIconIsVisible(): Promise<boolean> {
    return await this.profileIcon.isVisible();
  }

  async clickProfileLogout() {
    await this.profileIcon.hover();
    await this.logout.click();
  }

  async clickMyProfile() {
    await this.page.waitForSelector('//i[@class="hdr-user-avatar-icon-bc bc-i-user user-not-verified"]');
    await this.profileIcon.hover();
    await this.page.waitForSelector('//span[normalize-space()="My Profile"]');
    await this.myProfile.click();
  }
}
