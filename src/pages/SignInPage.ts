import {expect, Locator, Page} from '@playwright/test'

export class SignInPage {
  private page: Page

  private readonly email: Locator
  private readonly password: Locator
  private readonly rememberMeCheckbox: Locator
  private readonly signInButton: Locator

  private readonly errorRequiredEmail: Locator
  private readonly errorRequiredPassword: Locator
  private readonly errorMessage: Locator
  private readonly closeVerificationPopUp: Locator

  constructor(page: Page) {
    this.page = page

    this.email = this.page.locator('//input[@name="username"]')
    this.password = this.page.locator('//input[@name="password"]')
    this.rememberMeCheckbox = this.page.locator('//i[@class="checkbox-control-icon-bc bc-i-checked"]')
    this.signInButton = this.page.locator('//button[@type="submit"]//span[contains(text(),"Sign in")]')

    this.errorRequiredEmail = this.page.locator('//input[@name="username"]/../..//span[@class="form-control-message-bc"]')
    this.errorRequiredPassword = this.page.locator('//input[@name="password"]/../..//span[@class="form-control-message-bc"]')
    this.errorMessage = this.page.locator('//div[@class="entrance-f-error-message-bc"]')
    this.closeVerificationPopUp = this.page.locator('//div[@class="popup-holder-bc windowed   info"]//i[@class="e-p-close-icon-bc bc-i-close-remove"]')
  }

  async fillSignInForm(email: string, password: string) {
    await this.email.fill(email);
    await this.password.fill(password);
  }

  async clickSignInButton() {
    await this.signInButton.click();
  }

  async clickRememberMeCheckboxTrue() {
    await this.rememberMeCheckbox.check();
  }

  async assertSignInErrorVisible() {
    await expect(this.errorMessage).toBeVisible();
  }

  async assertRequiredEmailErrorVisible() {
    await expect(this.errorRequiredEmail).toBeVisible();
  }

  async assertRequiredPasswordErrorVisible() {
    await expect(this.errorRequiredPassword).toBeVisible();
  }

  async clickCloseVerificationPopUp() {
    await this.closeVerificationPopUp.click();
  }
}
